`# Document d’architecture technique

## Les besoins fonctionnels

Déployer un pipeline de déploiement continu pour l’application Azure Voting App et sa base de données Redis sur le cluster Kubernetes de Azure : AKS.
À chaque mise à jour de l’application, le pipeline devra automatiquement déployer la mise à jour à place de l’ancienne version.
L’image docker de l’application sera mise à jour toutes les heures par un push dans son dépôt DockerHub : https://hub.docker.com/repository/docker/simplonasa/azure_voting_app

## Les besoins non fonctionnels

| Elements | Valeurs | Commentaires |
| --- | --- | --- |
| Virtual Machine | Debian 11 | Applicatif Jenkins |
| Cluster Kubernetes | AKS | VotingApp |


## Représentation fonctionnelle
![](https://i.imgur.com/HaMUg0V.jpg)


## Représentation applicative
![](https://i.imgur.com/jEOZnRm.jpg)


## Représentation infrastructure
![](https://i.imgur.com/mr2fYRe.jpg)


## Décisions d'architecture

| Sujet | Contenu |
| ----- | ------- |
| Décisions d'architecture | S'appuyer sur une pipeline |
| Problématique | Quel pipeline choisir ? |
| Hypothése | Il existe plusieurs applicatif qui peuvent fournir des pipelines CI/CD |
| Alternative | Option 1: Jenkins <br /> Option 2: gitlab CI/CD|
| Decisions | Jenkins |
| Justification | J'ai plus de compétence sur Jenkins, et Jenkins est totalement gratuit. |
| Implication | Installation de Jenkins nécessaire |

## Les coûts

**Mensuel/En Dollar.**

* Avec engagement sur 3 ans:
  * Réseau virtuel: 14.00
  * Adresse IP: 2.63
  * Machine virtuelle: 55.20
  * AKS: 37.05

Total estimé: 108.88

* Avec engagement sur 1 an:
  * Réseau virtuel: 14.00
  * Adresse IP: 2.63
  * Machine virtuelle: 83.34
  * AKS: 55.09

Total estimé: 155.05

* Sans engagement:
  * Réseau virtuel: 96.36
  * Adresse IP: 2.63
  * Machine virtuelle: 135.05
  * AKS: 37.05

Total estimé: 248.04 

